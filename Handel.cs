using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using DialogSystem.Runtime;

public class Handel : MonoBehaviour
{
    DialogModule dialogModule;

    public GameObject handel;
    // Start is called before the first frame update
    void Start()
    {
        dialogModule = GetComponent<DialogModule>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OpenHandelWindow()
    {
        handel.SetActive(true);
    }
}
