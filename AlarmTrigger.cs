using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AlarmTrigger : MonoBehaviour
{
    [SerializeField] private float alarmRange = 40;

    BehaviorController behaviorController;

    // Start is called before the first frame update
    private void Start()
    {
        behaviorController = GetComponent<BehaviorController>();
    }
    public void Alarm(Transform target)
    {
        Collider[] hitColiders = Physics.OverlapSphere(transform.position, alarmRange);
        foreach (var hitColider in hitColiders)
        {
            if (hitColider.CompareTag("Enemy"))
            {
                behaviorController.AlarmChecker(hitColider.transform);
            }
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, alarmRange);
    }
    }
