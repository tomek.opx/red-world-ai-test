using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BehaviorController : MonoBehaviour
{
    // Attack
    [SerializeField] private float attackRange = 3f;
    private bool playerInSightRange, playerInAttackRange;

    //sight
    [SerializeField] private float sightRange, fieldOfViewAngle = 130f, losRadius = 45f;


    //memory
    private bool aiMemorizedPlayer = false;
    [SerializeField] private float memoryStartTime = 10f;
    private float increasingMemoryTime;

    //hearing
    Vector3 noisePosition;
    private bool aiHeardPlayer = false, canSpin = false;
    [SerializeField] private float noiseTravelDistance = 30f, spinSpeed = 3f, spinTime = 3f;
    private float isSpiningTime;

    //stats
    private int currentHealth;
    [SerializeField] private int maxHealth = 50;

    bool alreadyAttacked = false, playerIsInLOS = false;

    //patroling
    public List<Transform> wayPointList;
    private int nextWayPoint;

    public Transform eyes;
    //navigation
    [SerializeField] NavMeshAgent enemy;
    [SerializeField] LayerMask playerMask;
    [HideInInspector] public Transform target;

    //preferences
    [SerializeField] private CharacterPref preferences;

    //alarm
    private bool alarmed, madeAlarm;
    Vector3 alarmPosition;
    [SerializeField] private float alarmRange = 40;


    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        Patroling();
    }
    private void Update()
    {
        if (!preferences.isAgressiv && preferences.canSpeak) Debug.Log("nic");
        else if (preferences.isAgressiv)
        {
            if (alarmed) ChasePlayer(target);
            /* // last tested - worked
            playerInSightRange = Physics.CheckSphere(eyes.position, sightRange, playerMask);

            if(!playerInSightRange) Patroling();

            if (playerInSightRange) CheckLOS();

            if (!preferences.isAgressiv && preferences.canSpeak) Debug.Log("nic"); */

            playerInSightRange = Physics.CheckSphere(eyes.position, sightRange, playerMask);

            if (playerInSightRange) CheckLOS();

            if (!playerInSightRange && !aiMemorizedPlayer)
            {
                Patroling();
                StopCoroutine("AiMemory");
            }
            if (playerIsInLOS)
            {
                // check distance
                // if in attack range -> attack
                //else memory = true, lock at and chase
            }
            if (aiMemorizedPlayer && !playerIsInLOS)
            {
                ChasePlayer(target);
                StopCoroutine(AiMemory());
            }

        }
    }
    #region other features
    private void GoToNoisePosition()
    {
        enemy.SetDestination(noisePosition);
        if (Vector3.Distance(transform.position, noisePosition) <= 5f && canSpin)
        {
            isSpiningTime += Time.deltaTime;

            transform.Rotate(Vector3.up * spinSpeed, Space.World);
            if (isSpiningTime >= spinTime)
            {
                canSpin = false;
                aiHeardPlayer = false;
                isSpiningTime = 0f;
            }
        }
    }
    private IEnumerator AiMemory()
    {
        increasingMemoryTime = 0;
        while (increasingMemoryTime < memoryStartTime)
        {
            increasingMemoryTime += Time.deltaTime;
            aiMemorizedPlayer = true;
            yield return null;
        }
        aiHeardPlayer = false;
        aiMemorizedPlayer = false;
    }
    private void NoiseCheck()
    {
        float distance = Vector3.Distance(target.position, transform.position);
        if (distance <= noiseTravelDistance)
        {
            if (Input.GetButton("Fire1"))
            {
                noisePosition = target.position;

                aiHeardPlayer = true;
            }
            else
            {
                aiHeardPlayer = false;
                canSpin = false;
            }
        }
    }
    #endregion

    #region takeDamage and die
    public void TakeDamage(int dmg)
    {
        currentHealth -= dmg;


        alarmed = true;

        if (currentHealth <= 0)
        {
            Die();
        }
    }
    private void Die()
    {


        Destroy(gameObject);
    }
    #endregion

    #region states
    private void Attack(Transform target)
    {

        enemy.SetDestination(transform.position);
        transform.LookAt(target.position);
        Debug.Log(target.transform.name +" already attacked");

    }
    private void Patroling()
    {
        enemy.destination = wayPointList[nextWayPoint].position;
        enemy.Resume();
        if (enemy.remainingDistance <= enemy.stoppingDistance && !enemy.pathPending)
        {
            nextWayPoint = (nextWayPoint + 1) % wayPointList.Count;
        }
    }
    private void ChasePlayer(Transform target)
    {
        alarmPosition = target.position;
        enemy.SetDestination(alarmPosition);
        //enemy.SetDestination(target.position);
    }
    #endregion

    #region Gizmos
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, alarmRange);
    


    RaycastHit hit;
        bool isHit = Physics.SphereCast(transform.position, transform.lossyScale.x / 2, transform.forward, out hit, sightRange);
        if (isHit)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(eyes.position, eyes.forward * hit.distance);
            Gizmos.DrawWireSphere(eyes.position + eyes.forward * hit.distance, eyes.lossyScale.x /2);
        }
        else
        {
            Gizmos.color = Color.green;
            Gizmos.DrawRay(eyes.position, eyes.forward * sightRange);
        }
    }
    #endregion

    void CheckLOS()
    {
        Collider[] hitColliders = Physics.OverlapSphere(eyes.position, sightRange);
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.CompareTag("Player"))
            {
                Debug.Log("in my radius " + hitCollider.transform.name);

                Vector3 direction = hitCollider.transform.position - transform.position;

                float angle = Vector3.Angle(direction, transform.forward);

                if (angle < fieldOfViewAngle * 0.5f)
                {
                    //////// ---------->
                    if (Vector2.Distance(hitCollider.transform.position, eyes.transform.position) <= attackRange)
                    {
                        Attack(hitCollider.transform);
                    }
                    else ////////<----------
                    {
                        Debug.Log("player in LOS");
                        aiMemorizedPlayer = true;
                        transform.LookAt(hitCollider.transform.position);
                        ChasePlayer(hitCollider.transform);

                        if (!madeAlarm)
                        {
                            Alarm(hitCollider.transform);
                            Debug.Log(transform.name + "Wszczynam alarm");
                            madeAlarm = true;
                        }
                    }

                }

            }else if(hitCollider.tag != "Player")
            {
                Patroling();
            }
        }
    }

    #region alarm
    public void AlarmChecker(Transform alarmTarget)
    {
        alarmed = true;
        target = alarmTarget;
        Debug.Log(transform.name + " zostal zaalarmowany");
    }
    public void Alarm(Transform alarmPos)
    {
        Collider[] hitColiders = Physics.OverlapSphere(transform.position, alarmRange);
        foreach (var hitColider in hitColiders)
        {
            if (hitColider.CompareTag("Enemy"))
            {
                //alarmPosition = target.position;
                BehaviorController alarm = hitColider.GetComponent<BehaviorController>();
                alarm.ChasePlayer(alarmPos.transform);
            }
        }
    }
    #endregion
}

