using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Game.Input;
using System;

public class Interaction : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        

        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if(Physics.Raycast(ray, out hit, 100f))
        {
            if (hit.transform.CompareTag("NPC")) //&& Input.GetMouseButtonDown(0))
            {
                Cursor.visible = false;
                if(Input.GetMouseButtonDown(0))
                    Debug.Log("rozpoczynam dialog" + hit.transform.name);
            }
            if (hit.transform.CompareTag("Enemy"))
            {
                Debug.Log("Can attack" + hit.transform.name);

                // player attack here ?
            }
            else
                Cursor.visible = true;
        }
    }

}
