using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "EnemyAI/EnemyPreferences")]

public class CharacterPref : ScriptableObject
{
    [SerializeField] string name;
    public bool isAgressiv, canSpeak, canTrade, chaseingPlayer;
}
